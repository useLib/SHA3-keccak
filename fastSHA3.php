<?php /* -*- coding: utf-8; indent-tabs-mode: t; tab-width: 3 -*-
vim: ts=3 noet ai */

/**
	Streamable SHA-3 for PHP 5.2+, with no lib/ext dependencies!

	Copyright © 2018  Desktopd Developers

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	@license LGPL-3+
	@file
*/

/**
	SHA-3 (FIPS-202) for PHP strings (byte arrays) (PHP 5.2.1+)
	PHP 7.0 computes SHA-3 about 4 times faster than PHP 5.2 - 5.6 (on x86_64)
	
	Based on the reference implementations, which are under CC-0
	Reference: http://keccak.noekeon.org/
	
	This uses PHP's native byte strings. Supports 32-bit as well as 64-bit
	systems. Also for LE vs. BE systems.
*/

/**
	Optimized via several implementations of keccak rotations to benchmark them
	Copyright © 2019 by Dr. Dirk Fischer, use-Optimierung, Cologne, Germany
	The original is speeded up quite hard and runs 2.34 times faster now.
*/
class SHA3 {
	const SHA3_224 = 1;
	const SHA3_256 = 2;
	const SHA3_384 = 3;
	const SHA3_512 = 4;
	
	const SHAKE128 = 5;
	const SHAKE256 = 6;
	
	
	public static function init ($type = null) {
		switch ($type) {
			case self::SHA3_224: return new self (1152, 448, 0x06, 28);
			case self::SHA3_256: return new self (1088, 512, 0x06, 32);
			case self::SHA3_384: return new self (832, 768, 0x06, 48);
			case self::SHA3_512: return new self (576, 1024, 0x06, 64);
			case self::SHAKE128: return new self (1344, 256, 0x1f);
			case self::SHAKE256: return new self (1088, 512, 0x1f);
		}
		
		throw new Exception ('Invalid operation type');
	}
	
	
	/**
		Feed input to SHA-3 "sponge"
	*/
	public function absorb ($data) {
		if (self::PHASE_INPUT != $this->phase) {
			throw new Exception ('No more input accepted');
		}
		
		$rateInBytes = $this->rateInBytes;
		$this->inputBuffer .= $data;
		while (strlen ($this->inputBuffer) >= $rateInBytes) {
			list ($input, $this->inputBuffer) = array (
				substr ($this->inputBuffer, 0, $rateInBytes)
				, substr ($this->inputBuffer, $rateInBytes));
			$blockSize = $rateInBytes;
			for ($i = 0; $i < $blockSize; $i++) {
				$this->state[$i] = $this->state[$i] ^ $input[$i];
			}
			
			$this->state = self::keccakF1600Permute ($this->state);
		
			$this->blockSize = 0;
		}
		return $this;
	}
	
	/**
		Get hash output
	*/
	public function squeeze ($length = null) {
		$outputLength = $this->outputLength; // fixed length output
		if ($length && 0 < $outputLength && $outputLength != $length) {
			throw new Exception ('Invalid length');
		}
		
		if (self::PHASE_INPUT == $this->phase) {
			$this->finalizeInput ();
		}
		
		if (self::PHASE_OUTPUT != $this->phase) {
			throw new Exception ('No more output allowed');
		}
		if (0 < $outputLength) {
			$this->phase = self::PHASE_DONE;
			return $this->getOutputBytes ($outputLength);
		}
		
		$blockLength = $this->rateInBytes;
		list ($output, $this->outputBuffer) = array (
			substr ($this->outputBuffer, 0, $length)
			, substr ($this->outputBuffer, $length));
		$neededLength = $length - strlen ($output);
		$diff = $neededLength % $blockLength;
		if ($diff) {
			$readLength = (($neededLength - $diff) / $blockLength + 1)
				* $blockLength;
		} else {
			$readLength = $neededLength;
		}
		
		$read = $this->getOutputBytes ($readLength);
		$this->outputBuffer .= substr ($read, $neededLength);
		return $output . substr ($read, 0, $neededLength);
	}
	
	// internally used
	const PHASE_INIT = 1;
	const PHASE_INPUT = 2;
	const PHASE_OUTPUT = 3;
	const PHASE_DONE = 4;
	
	private $phase = self::PHASE_INIT;
	private $state; // byte array (string)
	private $rateInBytes; // positive integer
	private $suffix; // 8-bit unsigned integer
	private $inputBuffer = ''; // byte array (string): max length = rateInBytes
	private $outputLength = 0;
	private $outputBuffer = '';
	
	
	public function __construct ($rate, $capacity, $suffix, $length = 0) {
		if (1600 != ($rate + $capacity)) {
			throw new Error ('Invalid parameters');
		}
		if (0 != ($rate % 8)) {
			throw new Error ('Invalid rate');
		}
		
		$this->suffix = $suffix;
		$this->state = str_repeat ("\0", 200);
		$this->blockSize = 0;
		
		$this->rateInBytes = $rate / 8;
		$this->outputLength = $length;
		$this->phase = self::PHASE_INPUT;
		return;
	}
	
	protected function finalizeInput () {
		$this->phase = self::PHASE_OUTPUT;
		
		$input = $this->inputBuffer;
		$inputLength = strlen ($input);
		if (0 < $inputLength) {
			$blockSize = $inputLength;
			for ($i = 0; $i < $blockSize; $i++) {
				$this->state[$i] = $this->state[$i] ^ $input[$i];
			}
			$this->blockSize = $blockSize;
		}
		
		// Padding
		$rateInBytes = $this->rateInBytes;
		$this->state[$this->blockSize] = $this->state[$this->blockSize]
			^ chr ($this->suffix);
		if (($this->suffix & 0x80) != 0
			&& $this->blockSize == ($rateInBytes - 1)) {
			$this->state = self::keccakF1600Permute ($this->state);
		}
		$this->state[$rateInBytes - 1] = $this->state[$rateInBytes - 1] ^ "\x80";
		$this->state = self::keccakF1600Permute ($this->state);
	}
	
	protected function getOutputBytes ($outputLength) {
		// Squeeze
		$output = '';
		while (0 < $outputLength) {
			$blockSize = min ($outputLength, $this->rateInBytes);
			$output .= substr ($this->state, 0, $blockSize);
			$outputLength -= $blockSize;
			if (0 < $outputLength) {
				$this->state = self::keccakF1600Permute ($this->state);
			}
		}
		return $output;
	}

	/**
		1600-bit state version of Keccak's permutation
	*/
	protected static function keccakF1600Permute ($state) {
	// with all optimations 5.5sec for all test vectors instead of 12.9 => 2.34 times faster!
	// (on my old MacBook Pro, Mitte 2010: 2,53 GHz Intel Core i5)
		$rotL64 = function( $c, $b ) {
			$bb = $b%32;
			$bbQ = 32 - $bb;
			$save = $high = unpack("L", substr( $c, 0, 4 ));
			$low = unpack("L", substr( $c, -4 ));
			$high = ((($high[1] << $bb) & 0xffffffff)) ^ ($low[1] >> $bbQ);
			$low = ((($low[1] << $bb) & 0xffffffff)) ^ ($save[1] >> $bbQ);
			$high = pack("L", $high );
			$low = pack("L", $low );
			return  $b > 31 ? "$low$high" : "$high$low";
		};
		$a = str_split ($state, 8);
		$offsets = [	// precalculated offsets for ι step
			  [0]
			, [1, 7, 15]
			, [1, 3, 7, 15, 63]
			, [15, 31, 63]
			, [0, 1, 3, 7, 15]
			, [0, 31]
			, [0, 7, 15, 31, 63]
			, [0, 3, 15, 63]
			, [1, 3, 7]
			, [3, 7]
			, [0, 3, 15, 31]
			, [1, 3, 31]
			, [0, 1, 3, 7, 15, 31]
			, [0, 1, 3, 7, 63]
			, [0, 3, 7, 15, 63]
			, [0, 1, 15, 63]
			, [1, 15, 63]
			, [7, 63]
			, [1, 3, 15]
			, [1, 3, 31, 63]
			, [0, 7, 15, 31, 63]
			, [7, 15, 63]
			, [0, 31]
			, [3, 15, 31, 63]
		];
		$values = "\1\2\4\10\20\40\100\200";
		for ($round = 0; $round < 24; $round++) {
			// θ step
			$C = array ();
         $C[] = $a[0] ^ $a[5] ^ $a[10] ^ $a[15] ^ $a[20]; // $all + 0
         $C[] = $a[1] ^ $a[6] ^ $a[11] ^ $a[16] ^ $a[21]; // $all + 1
         $C[] = $a[2] ^ $a[7] ^ $a[12] ^ $a[17] ^ $a[22]; // $all + 2
         $C[] = $a[3] ^ $a[8] ^ $a[13] ^ $a[18] ^ $a[23]; // $all + 3
         $C[] = $a[4] ^ $a[9] ^ $a[14] ^ $a[19] ^ $a[24]; // $all + 4

			$D = array ();
			$D[] = $C[4] ^ $rotL64 ($C[1], 1);
			$D[] = $C[0] ^ $rotL64 ($C[2], 1);
			$D[] = $C[1] ^ $rotL64 ($C[3], 1);
			$D[] = $C[2] ^ $rotL64 ($C[4], 1);
			$D[] = $C[3] ^ $rotL64 ($C[0], 1);

			for ($x = 0; $x < 5; $x++) {
				$a[$x] = $a[$x] ^ $D[$x];
				$a[5+$x] = $a[5+$x] ^ $D[$x];
				$a[10+$x] = $a[10+$x] ^ $D[$x];
				$a[15+$x] = $a[15+$x] ^ $D[$x];
				$a[20+$x] = $a[20+$x] ^ $D[$x];
			}
			unset ($C, $D);
			
			// ρ and π steps
			$current = $a[1]; // x, y
			$steps = [
			  10 =>  1,  7 =>  3, 11 =>  6, 17 => 10, 18 => 15,  3 => 21,  5 => 28, 16 => 36
			,  8 => 45, 21 => 55, 24 =>  2,  4 => 14, 15 => 27, 23 => 41, 19 => 56, 13 =>  8
			, 12 => 25,  2 => 43, 20 => 62, 14 => 18, 22 => 39,  9 => 61,  6 => 20,  1 => 44
			];
			foreach( $steps as $idx => $shift )
				list ($current, $a[$idx]) = [ $a[$idx], $rotL64 ($current, $shift)];
			
			// χ step
			for ($y = 0; $y < 25; $y+=5) {
				$t = array_slice( $a, $y, 5 );
				$a[$y] = $t[0] ^ ((~ $t[1]) & $t[2]);
				$a[$y+1] = $t[1] ^ ((~ $t[2]) & $t[3]);
				$a[$y+2] = $t[2] ^ ((~ $t[3]) & $t[4]);
				$a[$y+3] = $t[3] ^ ((~ $t[4]) & $t[0]);
				$a[$y+4] = $t[4] ^ ((~ $t[0]) & $t[1]);
			}
			// ι step
			foreach( $offsets[ $round ] as $j => $offset) {
				$shift = $offset % 8;
				$n = "\0\0\0\0\0\0\0\0";
				$n[($offset - $shift) / 8] = $values[$shift];				
				$a[0] = $a[0] ^ $n;
			}
		}
		
		return implode ($a);
	}	
}
?>