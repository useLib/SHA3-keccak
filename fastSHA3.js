/**
   Streamable SHA-3 for javascript, with no lib/ext dependencies!

   Copyright © 2018  Desktopd Developers PHP script
   © 2019 translated and optimized by Dr. D. Fischer, Cologne (use-Optimierung)

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

   @license LGPL-3+
   @file
*/

/**
   SHA-3 (FIPS-202) for javascript (arrays of byte values)

   all operations are recreated for arrays of byte values. The given string is converted
   into it in the first step and the 'hash array of byte' is reconverted into a string
   by returning it. Methods on strings are translated into array operations and string
   appends have to be done as concat operations. According to byte length on bit
   operations the results allways have to be masked with &0xff. Stored between arrays
   are copied via a.slice(0). Constructions with list() are done in different steps, if
   nessesary with a temporary keep of a value between. 
   
   Mainly this translation from php is done to have two identic, high quality hashing
   methods on client and server to hash symmetric secrets. Therefore the client side
   does not need such a high performance as the serverside, at least it has.
   At least the performance is quite ok, because all keccak operations do not need
   conversions and native array methods could be used in many cases.

   Based on the php implementation php-sha3-streamable
   Reference: https://notabug.org/desktopd/php-sha3-streamable
   Based on the reference implementations, which are under CC-0
   Reference: http://keccak.noekeon.org/
   
   REMARK: it is nessesary to use a 'old' class definition because ES2015 classes
   are not supported by IE.
   For the same reason Uint8Array is not used (and on performance reasons)
   The 'translation' was done in two steps to make it traceable. First all as simular
   to the original as possible. Second (the javascript to use) eliminate some not
   nessesary variables, function calls and optimization of a few loops.
*/

(function( _root ) {
   function _SHA3 () {

      this.SHA3_224 = 1;
      this.SHA3_256 = 2;
      this.SHA3_384 = 3;
      this.SHA3_512 = 4;
      this.SHAKE128 = 5;
      this.SHAKE256 = 6;

   };
   
   _SHA3.prototype.init = function(type = null) {
      var private = this.private;
      switch (type) {
         case this.SHA3_224: return private.__construct (1152, 448, 0x06, 28);
         case this.SHA3_256: return private.__construct (1088, 512, 0x06, 32);
         case this.SHA3_384: return private.__construct (832, 768, 0x06, 48);
         case this.SHA3_512: return private.__construct (576, 1024, 0x06, 64);
         case this.SHAKE128: return private.__construct (1344, 256, 0x1f);
         case this.SHAKE256: return private.__construct (1088, 512, 0x1f);
      }
      throw new Error ('Invalid operation type');
   };
   
   /**
      Feed input to SHA-3 "sponge"
   */
   _SHA3.prototype.absorb = function( data ) {
      var private = this.private
      , input
      ;
      if (PHASE_INPUT != private.phase) {
         throw new Error ('No more input accepted');
      }
      private.inputBuffer = private.inputBuffer.concat( str2array (data));
      while ( private.inputBuffer.length >= private.rateInBytes) {
         input = private.inputBuffer.slice( 0, private.rateInBytes );
         private.inputBuffer = private.inputBuffer.slice( private.rateInBytes );
         for ( var i = 0; i < private.rateInBytes; i++) {
            private.state[i] = (private.state[i] ^ input[i]) & 0xff;
         }
         private.state = keccakF1600Permute (private.state);
         private.blockSize = 0;
      }
      return this;
   };
   
   /**
      Get hash output
   */
   _SHA3.prototype.squeeze = function (length) {
      var private = this.private
      , outputLength = private.outputLength; // fixed length output
      length = length || null;
      if (length && 0 < outputLength && outputLength != length) {
         throw new Error ('Invalid length');
      }
      
      if (PHASE_INPUT == private.phase) {
         private.finalizeInput ();
      }
      
      if (PHASE_OUTPUT != private.phase) {
         throw new Error ('No more output allowed');
      }
      if (0 < outputLength) {
         private.phase = PHASE_DONE;
         return array2str( private.getOutputBytes (outputLength));
      }

      var blockLength = private.rateInBytes
      , output = private.outputBuffer.slice( 0, length )
      , neededLength = length - output.length
      , diff = neededLength % blockLength
      ;
      private.outputBuffer = private.outputBuffer.slice( length );
      if (diff) {
         readLength = ((neededLength - diff) / blockLength + 1)
            * blockLength;
      } else {
         readLength = neededLength;
      }
      
      var read = private.getOutputBytes (readLength);
      private.outputBuffer = private.outputBuffer.concat( read.slice( neededLength ));
      return array2str( output.concat( read.slice( 0, neededLength )));
   };

   // internally used
   var PHASE_INIT = 1
   , PHASE_INPUT = 2
   , PHASE_OUTPUT = 3
   , PHASE_DONE = 4
   ;

   _SHA3.prototype.private = {

         __construct: function (rate, capacity, suffix, length) {
         if (1600 != (rate + capacity)) {
            throw new Error ('Invalid parameters');
         }
         if (0 != (rate % 8)) {
            throw new Error ('Invalid rate');
         }
         
         this.state = (function( c, a ) {
            while(c--)
               a.push(0);
            return a;
         })( 200, [] );
         this.suffix = suffix;
         this.blockSize = 0;
         this.rateInBytes = rate / 8;
         this.outputLength = length || 0;
         this.phase = PHASE_INPUT;
         this.inputBuffer = [];
         this.outputBuffer = [];

         return new _SHA3();
      }

      , finalizeInput: function () {
         this.phase = PHASE_OUTPUT;
         
         if (0 < this.inputBuffer.length) {
            for ( var i = 0; i < this.inputBuffer.length; i++) {
               this.state[i] = (this.state[i] ^ this.inputBuffer[i]) & 0xff;
            }
            this.blockSize = this.inputBuffer.length;
         }
         
         // Padding
         var rateInBytes = this.rateInBytes;
         this.state[this.blockSize] = (this.state[this.blockSize] ^ this.suffix) & 0xff;
         if ((this.suffix & 0x80) != 0
         && this.blockSize == (rateInBytes - 1)) {
            this.state = keccakF1600Permute (this.state);
         }
         this.state[rateInBytes - 1] = this.state[rateInBytes - 1] ^ 0x80;
         this.state = keccakF1600Permute (this.state);
      }

      , getOutputBytes: function (outputLength) {
         // Squeeze
         var output = []
         , blockSize
         ;
         while (0 < outputLength) {
            blockSize = Math.min (outputLength, this.rateInBytes);
            output = output.concat (this.state.slice (0, blockSize));
            outputLength -= blockSize;
            if (0 < outputLength) {
               this.state = keccakF1600Permute (this.state);
            }
         }
         return output;
      }
   }
   
   _root.SHA3 = new _SHA3();

   // translated functions
   var str2array = function( s ) {
      //for (var i=0, l=s.length, a=[]; i<l; a.push( s.charCodeAt (i++)));
      for (var i=0, l=s.length, a=[]; i<l; a[ i ] = s.charCodeAt( i++ ));
      return a;
   }
   , array2str = function( a ) {
      for (var i=0, l=a.length, s=''; i<l; s+=String.fromCharCode (a[i++]));
      return s;
   }
   , xor = function( a, b ) {
      for (var i=0, aa=[]; i<8; aa[i] = (a[i] ^ b[i++]) & 0xff);
      return aa;
   }
   , and = function( a, b ) {
      for (var i=0, aa=[]; i<8; aa[i] = (a[i] & b[i++]) & 0xff);
      return aa;
   }
   , not = function( a ) {
      for (var i=8, aa=[]; i>=0; i--)
         aa[i] = ~a[i] & 0xff;
      return aa;
   }
   , rotL64Low = function( n ) {
   // 64-bit bitwise left rotation (Little endian)
   return [].concat (
         (n[0] << 1) & 0xff ^ (n[7] >> 7)
         , (n[1] << 1) & 0xff ^ (n[0] >> 7)
         , (n[2] << 1) & 0xff ^ (n[1] >> 7)
         , (n[3] << 1) & 0xff ^ (n[2] >> 7)
         , (n[4] << 1) & 0xff ^ (n[3] >> 7)
         , (n[5] << 1) & 0xff ^ (n[4] >> 7)
         , (n[6] << 1) & 0xff ^ (n[5] >> 7)
         , (n[7] << 1) & 0xff ^ (n[6] >> 7)
      );
   }
   , rotL64 = function (n, offset) {
   // 64-bit bitwise left rotation (Little endian)
      var shift = offset % 8
      , octetShift = (offset - shift) / 8
      , overflow = 0x00
      ;
      n = n.slice (- octetShift).concat( n.slice( 0, - octetShift))
      for ( var i = 0; i < 8; i++) {
         a = n[i] << shift;
         n[i] = (0xff & a) | overflow;
         overflow = a >> 8;
      }
      n[0] = n[0] | overflow;
      return n;
   }
   ;
   /**
      1600-bit state version of Keccak's permutation
   */
   function keccakF1600Permute (state) {
   var
      lanes = (function( s, c ) {
      // WATCH IT!! :: s is deleted, but on state used only, so its given back in a...
      var a = [];
         c = c || 1;
         while( s.length )
            a.push( s.splice( 0, c ));
         return a;
      })( state, 8 )
      , R = 1
      , values = [1,2,4,8,16,32,64,128]
      , x, y, idx
      ;
      for ( var round = 0, C, D, temp; round < 24; round++) {
         // θ step
         C = [];
         for ( x = 0; x < 5; x++) {
            // (x, 0) (x, 1) (x, 2) (x, 3) (x, 4)
            C[x] = xor( xor( xor( xor(lanes[x], lanes[x + 5]), lanes[x + 10])
               , lanes[x + 15]), lanes[x + 20]);
         }
         for ( x = 0; x < 5; x++) {
            D = xor( C[(x + 4) % 5], rotL64Low (C[(x + 1) % 5]));
            for ( y = 0, idx; y < 5; y++) {
               idx = x + 5 * y; // x, y
               lanes[idx] = xor( lanes[idx], D);
            }
         }
         C=D=null;
         
         // ρ and π steps
         x = 1;
         y = 0;
         for ( var i = 0, current = lanes[1].slice(0), keep; i < 24; i++) {
            keep = x;
            x = y;
            y = (2 * keep + 3 * y) % 5;
            idx = x + 5 * y;
            keep = current.slice(0);
            current = lanes[idx].slice(0);
            lanes[idx] = rotL64 (keep, ((i + 1) * (i + 2) / 2) % 64);
         }
         temp = current = null;
         
         // χ step
         temp = [];
         for ( var y = 0; y < 5; y++) {
            for ( var x = 0; x < 5; x++) {
               temp[x] = lanes[x + 5 * y].slice(0);
            }
            for ( var x = 0; x < 5; x++) {
               lanes[x + 5 * y] = xor (temp[x]
                  , and (not (temp[(x + 1) % 5]), temp[(x + 2) % 5]));
            }
         }
         temp = null;
         
         // ι step
         for ( var j = 0, offset, shift, octetShift; j < 7; j++) {
            R = ((R << 1) ^ ((R >> 7) * 0x71)) & 0xff;
            if (R & 2) {
               offset = (1 << j) - 1;
               shift = offset % 8;
               octetShift = (offset - shift) / 8;
               n = [0,0,0,0,0,0,0,0];
               n[octetShift] = values[shift];
               
               lanes[0] = xor (lanes[0], n);
            }
         }
      }
      return Array.prototype.concat.apply ([], lanes);
   }
})( window );
